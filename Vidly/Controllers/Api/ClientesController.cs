﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using Vidly.Dtos;
using Vidly.Models;

namespace Vidly.Controllers.API
{
    public class ClientesController : ApiController
    {
        private BDContexto _context;

        public ClientesController()
        {
            _context = new BDContexto();
        }

        //get /api/clientes
        public IEnumerable<ClienteDto> GetClientes()
        {
            return _context.Clientes.ToList().Select(Mapper.Map<Cliente, ClienteDto>);
        }

        //get /api/clientes/1
        public IHttpActionResult GetCliente(int id)
        {
            var cliente = _context.Clientes.SingleOrDefault(c => c.Id == id);

            if (cliente == null)
                return NotFound();

            return Ok(Mapper.Map<Cliente, ClienteDto>(cliente));
        }

        //post /api/clientes
        [HttpPost]
        public IHttpActionResult CreateCliente(ClienteDto clienteDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var cliente = Mapper.Map<ClienteDto, Cliente>(clienteDto);

            _context.Clientes.Add(cliente);
            _context.SaveChanges();

            clienteDto.Id = cliente.Id;

            return Created(new Uri(Request.RequestUri + "/" + cliente.Id), clienteDto);
        }

        //put /api/cliente/1
        [HttpPut]
        public void UpdateCliente(int id, ClienteDto clienteDto)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var clienteInBd = _context.Clientes.SingleOrDefault(c => c.Id == id);

            if (clienteInBd == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            Mapper.Map(clienteDto, clienteInBd);
            //clienteInBd.DataDeNascimento = clienteDto.DataDeNascimento;
            //clienteInBd.InscritoParaNovasNoticias = clienteDto.InscritoParaNovasNoticias;
            //clienteInBd.Nome = clienteDto.Nome;
            //clienteInBd.TipoDeMembroId = clienteDto.TipoDeMembroId;

            _context.SaveChanges();

        }

        //delete /api/clientes/1
        [HttpDelete]
        public void DeleteCliente(int id)
        {
            var clienteInBd = _context.Clientes.SingleOrDefault(c => c.Id == id);

            if (clienteInBd == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            _context.Clientes.Remove(clienteInBd);
            _context.SaveChanges();

        }



    }
}
