﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;

namespace Vidly.Controllers
{
    public class FilmesController : Controller
    {
        private BDContexto _context;

        public FilmesController()
        {
            _context = new BDContexto();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public ActionResult ListarFilmes()
        {
            var filmes = _context.Filmes.Include(f => f.GeneroDoFilme).ToList();

            return View(filmes);
        }

        public ActionResult FilmeForm()
        {
            var gnDoFilme = _context.GeneroDoFilme.ToList();

            var viewModel = new FilmeFormViewModel()
            {
                
                GeneroDoFilme = gnDoFilme
            };

            return View(viewModel);
        }

        public ActionResult NovoFilme()
        {
            var gnDoFilme = _context.GeneroDoFilme.ToList();

            var viewModel = new FilmeFormViewModel()
            {
                
                GeneroDoFilme = gnDoFilme
            };

            return View("FilmeForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SalvarFilme(Filme filme)
        {

            if (!ModelState.IsValid)
            {
                var viewModel = new FilmeFormViewModel(filme)
                {
                    
                    GeneroDoFilme = _context.GeneroDoFilme.ToList()
                };

                return View("FilmeForm", viewModel);
            }

            if (filme.Id == 0)
            {
                filme.DataDeAdicao = DateTime.Now;

                _context.Filmes.Add(filme);
            }
            else
            {
                var filmeInBd = _context.Filmes.SingleOrDefault(f => f.Id == filme.Id);

                filmeInBd.DataDeLancamento = filme.DataDeLancamento;
                filmeInBd.GeneroDoFilmeId = filme.GeneroDoFilmeId;
                filmeInBd.Nome = filme.Nome;
                filmeInBd.NumeroEmEstoque = filme.NumeroEmEstoque;

            }

            _context.SaveChanges(); // salva na base de dados
            
            return RedirectToAction("ListarFilmes", "Filmes");
        }

        public ActionResult EditarFilme(int id)
        {
            var filme = _context.Filmes.SingleOrDefault(f => f.Id == id);

            if (filme == null)
                return HttpNotFound();

            var viewModel = new FilmeFormViewModel(filme)
            {   
                GeneroDoFilme = _context.GeneroDoFilme.ToList()   
            };

            return View("FilmeForm", viewModel);
        }

        public ActionResult DetalharFilmes(int id)
        {
            var filme = _context.Filmes.Include(c => c.GeneroDoFilme).SingleOrDefault(c => c.Id == id);

            if (filme == null)
                return HttpNotFound();

            return View(filme);
        }

    }
}