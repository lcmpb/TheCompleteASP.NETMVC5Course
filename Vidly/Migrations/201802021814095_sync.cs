namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sync : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clientes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 255),
                        InscritoParaNovasNoticias = c.Boolean(nullable: false),
                        TipoDeMembroId = c.Byte(nullable: false),
                        DataDeNascimento = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TipoDeMembroes", t => t.TipoDeMembroId, cascadeDelete: true)
                .Index(t => t.TipoDeMembroId);
            
            CreateTable(
                "dbo.TipoDeMembroes",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        Nome = c.String(),
                        TaxaDeInscricao = c.Short(nullable: false),
                        DuracaoEmMeses = c.Byte(nullable: false),
                        TaxaDeDesconto = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Filmes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false),
                        GeneroDoFilmeId = c.Int(nullable: false),
                        DataDeLancamento = c.DateTime(nullable: false),
                        DataDeAdicao = c.DateTime(nullable: false),
                        NumeroEmEstoque = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GeneroDoFilmes", t => t.GeneroDoFilmeId, cascadeDelete: true)
                .Index(t => t.GeneroDoFilmeId);
            
            CreateTable(
                "dbo.GeneroDoFilmes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Filmes", "GeneroDoFilmeId", "dbo.GeneroDoFilmes");
            DropForeignKey("dbo.Clientes", "TipoDeMembroId", "dbo.TipoDeMembroes");
            DropIndex("dbo.Filmes", new[] { "GeneroDoFilmeId" });
            DropIndex("dbo.Clientes", new[] { "TipoDeMembroId" });
            DropTable("dbo.GeneroDoFilmes");
            DropTable("dbo.Filmes");
            DropTable("dbo.TipoDeMembroes");
            DropTable("dbo.Clientes");
        }
    }
}
