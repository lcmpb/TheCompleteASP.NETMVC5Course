﻿using System;
using System.ComponentModel.DataAnnotations;
using Vidly.Models;

namespace Vidly.Dtos
{
    public class ClienteDto
    {
        public int Id { get; set; }

        [Required] //DataAnnotations
        [StringLength(255)] //numero maximo de caracteres
        public string Nome { get; set; }

        public bool InscritoParaNovasNoticias { get; set; }
        
        public byte TipoDeMembroId { get; set; }

        //[Min18Anos]
        public DateTime? DataDeNascimento { get; set; }

    }
}