﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Vidly.Models;

namespace Vidly.ViewModels
{
    public class FilmeFormViewModel
    {
        public IEnumerable<GeneroDoFilme> GeneroDoFilme { get; set; }

        

        public int? Id { get; set; }

        [Required]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Required]
        [Display(Name = "Gênero do Filme")]
        public int? GeneroDoFilmeId { get; set; }

        [Required]
        [Display(Name = "Data de Lançamento")]
        public DateTime DataDeLancamento { get; set; }

        [Required]
        [Range(1, 20)]
        [Display(Name = "Número em Estoque")]
        public int? NumeroEmEstoque { get; set; }

        public string Titulo
        {
            get
            {
                return Id != 0 ? "Editar Filme" : "Novo Filme";
            }
        }

        public FilmeFormViewModel()
        {
            Id = 0;
            DataDeLancamento = DateTime.Parse("01/01/1900");
        }

        public FilmeFormViewModel(Filme filme)
        {
            Id = filme.Id;
            Nome = filme.Nome;
            DataDeLancamento = filme.DataDeLancamento;
            NumeroEmEstoque = filme.NumeroEmEstoque;
            GeneroDoFilmeId = filme.GeneroDoFilmeId;

        }


    }
}