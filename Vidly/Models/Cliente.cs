﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vidly.Models
{
    public class Cliente
    {
        public int Id { get; set; }

        [Required] //DataAnnotations
        [StringLength(255)] //numero maximo de caracteres
        public string Nome { get; set; }

        public bool InscritoParaNovasNoticias { get; set; }
        public TipoDeMembro TipoDeMembro { get; set; }

        [Display(Name = "Tipo de Membro")]
        public byte TipoDeMembroId { get; set; }

        [Display(Name = "Data de Nascimento")]
        [Min18Anos]
        public DateTime? DataDeNascimento { get; set; }

    }
}