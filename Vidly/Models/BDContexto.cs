﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Vidly.Models
{
    public class BDContexto : DbContext 
    {
        public BDContexto(): base()
        {

        }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Filme> Filmes { get; set; }
        public DbSet<TipoDeMembro> TipoDeMembro { get; set; }
        public DbSet<GeneroDoFilme> GeneroDoFilme { get; set; }

    }
}