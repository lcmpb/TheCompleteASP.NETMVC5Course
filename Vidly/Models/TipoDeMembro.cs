﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vidly.Models
{
    public class TipoDeMembro
    {
        public byte Id { get; set; }
        public string Nome { get; set; }
        public short TaxaDeInscricao { get; set; }
        public byte DuracaoEmMeses { get; set; }
        public byte TaxaDeDesconto { get; set; }


        public static readonly byte Unknown = 0;
        public static readonly byte Mensal = 1;
    }
}