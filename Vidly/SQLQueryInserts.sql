﻿INSERT INTO [dbo].[TipoDeMembroes] ([Id], [Nome], [TaxaDeInscricao], [DuracaoEmMeses], [TaxaDeDesconto]) VALUES (1, N'Mensal', 1, 1, 10)
INSERT INTO [dbo].[TipoDeMembroes] ([Id], [Nome], [TaxaDeInscricao], [DuracaoEmMeses], [TaxaDeDesconto]) VALUES (2, N'BiMestral', 2, 2, 20)
INSERT INTO [dbo].[TipoDeMembroes] ([Id], [Nome], [TaxaDeInscricao], [DuracaoEmMeses], [TaxaDeDesconto]) VALUES (3, N'TriMestral', 3, 3, 30)

SET IDENTITY_INSERT [dbo].[GeneroDoFilmes] ON
INSERT INTO [dbo].[GeneroDoFilmes] ([Id], [Nome]) VALUES (1, N'Comédia')
INSERT INTO [dbo].[GeneroDoFilmes] ([Id], [Nome]) VALUES (2, N'Ação')
INSERT INTO [dbo].[GeneroDoFilmes] ([Id], [Nome]) VALUES (3, N'Família')
INSERT INTO [dbo].[GeneroDoFilmes] ([Id], [Nome]) VALUES (4, N'Romance')
SET IDENTITY_INSERT [dbo].[GeneroDoFilmes] OFF

SET IDENTITY_INSERT [dbo].[Filmes] ON
INSERT INTO [dbo].[Filmes] ([Id], [Nome], [NumeroEmEstoque], [GeneroDoFilmeId], [DataDeLancamento], [DataDeAdicao]) VALUES (1, N'Hangover-', 1, 1, N'1900-01-01 00:00:00', N'1900-01-01 00:00:00')
INSERT INTO [dbo].[Filmes] ([Id], [Nome], [NumeroEmEstoque], [GeneroDoFilmeId], [DataDeLancamento], [DataDeAdicao]) VALUES (2, N'Die Hard', 2, 2, N'1900-01-01 00:00:00', N'1900-01-01 00:00:00')
INSERT INTO [dbo].[Filmes] ([Id], [Nome], [NumeroEmEstoque], [GeneroDoFilmeId], [DataDeLancamento], [DataDeAdicao]) VALUES (3, N'The Terminator', 3, 3, N'1900-01-01 00:00:00', N'1900-01-01 00:00:00')
INSERT INTO [dbo].[Filmes] ([Id], [Nome], [NumeroEmEstoque], [GeneroDoFilmeId], [DataDeLancamento], [DataDeAdicao]) VALUES (4, N'Toy Story', 4, 4, N'1900-01-01 00:00:00', N'1900-01-01 00:00:00')
INSERT INTO [dbo].[Filmes] ([Id], [Nome], [NumeroEmEstoque], [GeneroDoFilmeId], [DataDeLancamento], [DataDeAdicao]) VALUES (6, N'TesteComedia', 54, 1, N'1994-10-11 00:00:00', NULL)
SET IDENTITY_INSERT [dbo].[Filmes] OFF

SET IDENTITY_INSERT [dbo].[Clientes] ON
INSERT INTO [dbo].[Clientes] ([Id], [Nome], [InscritoParaNovasNoticias], [TipoDeMembroId], [DataDeNascimento]) VALUES (1, N'Lucas', 1, 1, N'1994-10-11 00:00:00')
INSERT INTO [dbo].[Clientes] ([Id], [Nome], [InscritoParaNovasNoticias], [TipoDeMembroId], [DataDeNascimento]) VALUES (2, N'ClienteUm', 0, 2, NULL)
INSERT INTO [dbo].[Clientes] ([Id], [Nome], [InscritoParaNovasNoticias], [TipoDeMembroId], [DataDeNascimento]) VALUES (3, N'ClienteDois', 1, 3, NULL)
INSERT INTO [dbo].[Clientes] ([Id], [Nome], [InscritoParaNovasNoticias], [TipoDeMembroId], [DataDeNascimento]) VALUES (4, N'Teste_1', 1, 1, N'2001-01-01 00:00:00')
INSERT INTO [dbo].[Clientes] ([Id], [Nome], [InscritoParaNovasNoticias], [TipoDeMembroId], [DataDeNascimento]) VALUES (5, N'Teste_2', 1, 1, N'2001-01-01 00:00:00')
INSERT INTO [dbo].[Clientes] ([Id], [Nome], [InscritoParaNovasNoticias], [TipoDeMembroId], [DataDeNascimento]) VALUES (6, N'teste', 1, 2, N'1994-10-11 00:00:00')
INSERT INTO [dbo].[Clientes] ([Id], [Nome], [InscritoParaNovasNoticias], [TipoDeMembroId], [DataDeNascimento]) VALUES (7, N'nome', 0, 1, NULL)
INSERT INTO [dbo].[Clientes] ([Id], [Nome], [InscritoParaNovasNoticias], [TipoDeMembroId], [DataDeNascimento]) VALUES (8, N'nome', 1, 1, NULL)
SET IDENTITY_INSERT [dbo].[Clientes] OFF
